package com.starzy.hadoop.hdfs;

import java.util.ArrayList;
import java.util.List;

/**
 * 目录树
 */
public class FSDirectory {

    private String name;

    private List<FSDirectory> child = new ArrayList<FSDirectory>();

    public FSDirectory(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FSDirectory> getChild() {
        return child;
    }

    public void setChild(List<FSDirectory> child) {
        this.child = child;
    }

    @Override
    public String toString() {
        return "FSDirectory{" +
                "name='" + name + '\'' +
                ", child=" + child +
                '}';
    }

    public static void main(String[] args) {
        FSDirectory rootPath = new FSDirectory("/");
        FSDirectory node1 = new FSDirectory("/test");
        rootPath.child.add(node1);
        FSDirectory node_1 = new FSDirectory("/test/test");
        node1.child.add(node_1);
        System.out.println(rootPath.toString());
    }
}
