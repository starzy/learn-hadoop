package com.starzy.hadoop.rpc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.Server;

import java.io.IOException;

/***************************************************************
 * TODO_ZH https://www.cnblogs.com/starzy
 * @Description: Hadoop RPC 服务端实现，并编写创建目录方法提供调用
 * @Author: starzy
 * @Date: 2020/12/11
 */
public class RPCServer implements ClientProtocol {

    /**************************************************************
     * TODO_ZH https://www.cnblogs.com/starzy
     * @Description: 创建目录
     */
    public void mkDir(String path) {
        System.out.println("服务器端：" + path);
    }
    /**************************************************************
     * TODO_ZH https://www.cnblogs.com/starzy
     * @Description: 构建Hadoop RPC并启动
     */
    public static void main(String[] args) throws IOException {

        /************************************************************
         * TODO_ZH starzy https://www.cnblogs.com/starzy
         *  注释：构建 Hadoop RPC 服务端
         *  使用构建者模式进行构建 Hadoop RPC
         */
        Server server = new RPC.Builder(new Configuration())
                .setBindAddress("localhost")
                .setPort(9999)
                .setProtocol(ClientProtocol.class)
                .setInstance(new RPCServer())
                .build();
        System.out.println("服务端 Hadoop RPC 启动");

        /**************************************************************
         * TODO_ZH starzy https://www.cnblogs.com/starzy
         * 注释：启动服务端Hadoop RPC
         */
        server.start();
    }
}
