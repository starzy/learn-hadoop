package com.starzy.hadoop.rpc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

/***************************************************************
 *TODO-ZH starzy https://www.cnblogs.com/starzy
 * 注释： RPC 客户端
 */
public class RPCClient {
    public static void main(String[] args) throws IOException {
        /*****************************************************************************************************
         *TODO-ZH starzy https://www.cnblogs.com/starzy
         * 注释： 获取服务端代理（客户端）
         */
        ClientProtocol namenode = RPC.getProxy(ClientProtocol.class,
                1234L,
                new InetSocketAddress("localhost", 9999),
                new Configuration());

        /***************************************************************
         *TODO-ZH starzy https://www.cnblogs.com/starzy
         * 注释：调用服务端代码
         */
        namenode.mkDir("/user/local/test");
    }
}
