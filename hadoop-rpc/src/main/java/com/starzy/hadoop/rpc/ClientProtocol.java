package com.starzy.hadoop.rpc;

/**
 * @Author: starzy https://www.cnblogs.com/starzy
 * @Description: 实现Hadoop RPC协议
 * @Date: Created in 10:27 2020/12/15
 */
public interface ClientProtocol {
    long versionID = 1234L;

    /**
     * 创建目录
     */
    void mkDir(String path);
}
